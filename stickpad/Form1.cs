﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using Microsoft.Win32;

namespace stickpad
{
    public partial class Form1 : Form
    {
        private string[] dosya;
        private string newcolor="Black";
        private string newbackground = "#FCFDDF";
        Form2 About = new Form2();
        Font myFont = new Font("Microsoft Sans Serif", 8);
        System.ComponentModel.TypeConverter converter = System.ComponentModel.TypeDescriptor.GetConverter(typeof(Font));
        RegistryKey rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

        public Form1()
        {
            InitializeComponent();
        }

        private void closeApplicatinToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void newNoteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Note myForm;
            myForm = new Note();
            myForm.Text = Convert.ToString(DateTime.Now);
            if (topMostToolStripMenuItem.Checked)
                myForm.TopMost = Enabled;
            myForm.richTextBox1.Font = myFont;
            myForm.richTextBox1.ForeColor = ColorTranslator.FromHtml(newcolor);
            myForm.richTextBox1.BackColor = ColorTranslator.FromHtml(newbackground);
            myForm.Show();
            myForm.richTextBox1.Font = myFont;  //Sadece yukarıya yazmak anlamadığım bir sebepten yeterli olmuyor???

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            Hide();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            FileInfo configvarmi = new FileInfo("stickypad.xml");
            if (configvarmi.Exists)
            {
                XmlTextReader config = new XmlTextReader("stickypad.xml");
                while (config.Read())
                {
                    if (config.NodeType == XmlNodeType.Element)
                    {
                        if (config.Name == "Background")
                        {
                            newbackground = config.ReadString();
                        }
                        if (config.Name == "Color")
                        {
                            newcolor = config.ReadString();
                        }
                        if (config.Name == "Font")
                        {
                            myFont = (Font)converter.ConvertFromString(config.ReadString());
                        }
                        if (config.Name == "TopMost")
                        {
                            topMostToolStripMenuItem.Checked = Convert.ToBoolean(config.ReadString());
                        }
                    }
                }

                config.Close();
            }
            backgroundColorToolStripMenuItem.Text = "Background Color -> " + newbackground;
            colorToolStripMenuItem.Text = "Color -> " + newcolor;
            fontToolStripMenuItem.Text = "Font -> " + myFont.Name + " - " + myFont.Size.ToString();

            try
            {
                dosya = Directory.GetFiles(".");
            }
            catch (Exception Hata)
            {
                MessageBox.Show(Hata.Message, "Run Time Find Eror! You Can't Open '*.kyt' Files");
                return;
            }

            for (Int32 i = 0; i < dosya.Length; i++)
            {
                FileInfo fi = new FileInfo(dosya[i]);
                //dataGridView1.Rows.Add(fi.DirectoryName.Replace(comboBox1.Text, "\\"), fi.Name.Substring(0, fi.Name.IndexOf(".")), fi.Extension, fi.Length, fi.LastWriteTime);
                if (fi.Extension==".kyt")
                {
                    Note myForm;
                    myForm = new Note();
                    myForm.Text = fi.Name.Replace(".kyt","");
                    myForm.Text = myForm.Text.Replace(";",":");
                    myForm.richTextBox1.LoadFile(fi.Name);

                    FileInfo varmi = new FileInfo(fi.Name.Replace(".kyt", ".xml"));
                    if (varmi.Exists)
                    {
                        XmlTextReader config = new XmlTextReader(fi.Name.Replace(".kyt", ".xml"));
                        while (config.Read())
                        {
                            if (config.NodeType == XmlNodeType.Element)
                            {
                                if (config.Name == "LocationX")
                                {
                                    myForm.Left = Convert.ToInt32(config.ReadString());
                                }
                                if (config.Name == "LocationY")
                                {
                                    myForm.Top = Convert.ToInt32(config.ReadString());
                                }
                                if (config.Name == "SizeH")
                                {
                                    myForm.Height = Convert.ToInt32(config.ReadString());
                                }
                                if (config.Name == "SizeW")
                                {
                                    myForm.Width = Convert.ToInt32(config.ReadString());
                                }
                                if (config.Name == "TopMost")
                                {
                                    myForm.TopMost = Convert.ToBoolean(config.ReadString());
                                    if (myForm.TopMost)
                                        myForm.topMostToolStripMenuItem.Checked = true;
                                    else
                                        myForm.topMostToolStripMenuItem.Checked = false;
                                }
                                if (config.Name == "Background")
                                {
                                    myForm.richTextBox1.BackColor = ColorTranslator.FromHtml(config.ReadString());
                                }

                            }
                        }

                        config.Close();

                    }

                    myForm.Show();

                }
            }
            
            if (rkApp.GetValue("MyApp") == null)
            {
                runStickyPadAtStartupToolStripMenuItem.Checked = false;
            }
            else
            {
                runStickyPadAtStartupToolStripMenuItem.Checked = true;
            }
        }

        private void abutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            About.ShowDialog();
        }

        private void colorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                newcolor = ColorTranslator.ToHtml(colorDialog1.Color);
                colorToolStripMenuItem.Text = "Color -> " + newcolor;
                Form1_FormClosed(null, null);
            }

        }

        private void runStickyPadAtStartupToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (runStickyPadAtStartupToolStripMenuItem.Checked)
            {
                rkApp.SetValue("MyApp", Application.ExecutablePath.ToString());
            }
            else
            {
                rkApp.DeleteValue("MyApp", false);
            }
        }

        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            newNoteToolStripMenuItem_Click(sender,e);
        }

        private void fontToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (fontDialog1.ShowDialog() == DialogResult.OK)
            {
                myFont = (Font)fontDialog1.Font.Clone();
                fontToolStripMenuItem.Text = "Font -> " +  myFont.Name + " - " + myFont.Size.ToString();
                Form1_FormClosed(null, null);
            }

        }

        private void backgroundColorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                newbackground = ColorTranslator.ToHtml(colorDialog1.Color);
                backgroundColorToolStripMenuItem.Text = "Background Color -> " + newbackground;
                Form1_FormClosed(null,null);
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            //Stickypad config dosyası oluşturuluyor ve kaydediliyor.
            XmlTextWriter config = new XmlTextWriter("stickypad.xml", System.Text.Encoding.GetEncoding("windows-1254"));
            config.Formatting = Formatting.Indented;
            config.WriteStartDocument();
            config.WriteComment("StickyPad General Config XML");

            config.WriteStartElement("Config");

            config.WriteStartElement("Background");
            config.WriteString(newbackground);
            config.WriteEndElement();
            config.WriteStartElement("Color");
            config.WriteString(newcolor);
            config.WriteEndElement();
            config.WriteStartElement("Font");
            //config.WriteString(myFont.ToString());
            config.WriteString(converter.ConvertToString(myFont));
            config.WriteEndElement();
            config.WriteStartElement("TopMost");
            config.WriteString(topMostToolStripMenuItem.Checked.ToString());
            config.WriteEndElement();

            config.WriteEndElement();
            config.WriteEndDocument();
            config.Close();
        }

        private void topMostToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form1_FormClosed(null, null);
        }
    }
}
