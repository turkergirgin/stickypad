﻿using System;
using System.Windows.Forms;

namespace stickpad
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                VisitLink();
            }
            catch (Exception)
            {
                MessageBox.Show("Unable to open link that was clicked.");
            }
        }

        private void VisitLink()
        { 
            linkLabel1.LinkVisited = true;
            System.Diagnostics.Process.Start("https://gitlab.com/turkergirgin/stickypad");
        }

    }
}
