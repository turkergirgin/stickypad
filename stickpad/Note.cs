﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Xml;

namespace stickpad
{
    public partial class Note : Form
    {
        private bool kapat=false;
        int xoldpozisyon, yoldpozisyon;
        Alarm Alarm = new Alarm();
        DateTime alarmtime;

        public Note()
        {
            InitializeComponent();
        }

        private void Note_Deactivate(object sender, EventArgs e)
        {
            if (kapat)  //Eğer kapat değişkeni true ise pencere kapatılırken dosyalar silinecek
            {
                File.Delete(Text.Replace(":", ";") + ".kyt");  //Note içerik dosyası siliniyor
                File.Delete(Text.Replace(":", ";") + ".xml");  //Note config dsyası siliniyor

                kapat = false;  //Kapat değişkeni eski haline atanıyor.
                //DestroyHandle();
            }
            else  //Eğer kapat değişkeni False ise
            {
                richTextBox1.SaveFile(Text.Replace(":", ";") + ".kyt");  //Note içerik dosyası kaydediliyor.

                //Note config dosyası oluşturuluyor ve kaydediliyor.
                XmlTextWriter config = new XmlTextWriter(Text.Replace(":", ";") + ".xml", System.Text.Encoding.GetEncoding("windows-1254"));
                config.Formatting = Formatting.Indented;
                config.WriteStartDocument();
                config.WriteComment("StickyPad Note Config XML");

                config.WriteStartElement("Config");

                config.WriteStartElement("LocationX");
                config.WriteString(DesktopLocation.X.ToString());
                config.WriteEndElement();
                config.WriteStartElement("LocationY");
                config.WriteString(DesktopLocation.Y.ToString());
                config.WriteEndElement();
                config.WriteStartElement("SizeH");
                config.WriteString(Size.Height.ToString());
                config.WriteEndElement();
                config.WriteStartElement("SizeW");
                config.WriteString(Size.Width.ToString());
                config.WriteEndElement();
                config.WriteStartElement("TopMost");
                config.WriteString(TopMost.ToString());
                config.WriteEndElement();
                config.WriteStartElement("Background");
                config.WriteString(ColorTranslator.ToHtml(richTextBox1.BackColor));
                config.WriteEndElement();

                config.WriteEndElement();
                config.WriteEndDocument();
                config.Close();

            }
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (fontDialog1.ShowDialog() == DialogResult.OK)
            {
                if (richTextBox1.SelectedText != "")
                    richTextBox1.SelectionFont = fontDialog1.Font;
                else
                    richTextBox1.Font = fontDialog1.Font;
            }

        }

        private void Note_FormClosed(object sender, FormClosedEventArgs e)
        {
            kapat = true;    //Pencerenin kapatılması gerektiği değişkene atanıyor.
        }

        private void colorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                if (richTextBox1.SelectedText != "")
                    richTextBox1.SelectionColor = colorDialog1.Color;
                else
                    richTextBox1.ForeColor = colorDialog1.Color;
            }
        }

        private void topMostToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (topMostToolStripMenuItem.Checked)
            {
                TopMost = true;
                pictureBox1.Visible = true;
            }
            else
            {
                TopMost = false;
                pictureBox1.Visible = false;
            }
        }

        private void backGroundColorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                richTextBox1.BackColor = colorDialog1.Color;
            }

        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            xoldpozisyon = e.X;
            yoldpozisyon = e.Y;
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button.ToString() == "None")
            {
                xoldpozisyon = MousePosition.X;
                yoldpozisyon = MousePosition.Y;
            }
            if (e.Button.ToString() == "Left")
            {
                SetDesktopLocation(MousePosition.X - xoldpozisyon, MousePosition.Y - yoldpozisyon);
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if (richTextBox1.TextLength > 0)
            {
                // Initializes the variables to pass to the MessageBox.Show method.

                string message = "If you close the note data will be deleted. Are you going to continue?";
                string caption = "Attention! Data loss warning.";
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult result;

                // Displays the MessageBox.

                result = MessageBox.Show(message, caption, buttons, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);

                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    // Closes the parent form.
                    Close();
                }
            }
            else
                Close();

        }

        private void pictureBox3_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button.ToString() == "Left")
            {
                SetClientSizeCore(MousePosition.X-Location.X, MousePosition.Y-Location.Y);
            }
        }

        private void richTextBox1_BackColorChanged(object sender, EventArgs e)
        {
            panel1.BackColor = richTextBox1.BackColor;
        }

        private void Note_Activated(object sender, EventArgs e)
        {
            if (TopMost)
            {
                pictureBox1.Visible = true;
                topMostToolStripMenuItem.Checked = true;
            }
        }

        private void alarmToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            if (alarmToolStripMenuItem.Checked)
            {
                Alarm.dateTimePicker1.MinDate = DateTime.Now;
                Alarm.ShowDialog();
                if (Alarm.DialogResult == System.Windows.Forms.DialogResult.OK)
                {
                    alarmToolStripMenuItem.Text = "Alarm - " + Alarm.dateTimePicker1.Value.ToString("dd.MM.yyyy hh:mm");
                    alarmtime = Alarm.dateTimePicker1.Value;
                    pictureBox4.Visible = true;
                    timer1.Enabled = true;
                }
                else
                    alarmToolStripMenuItem.Checked = false;
            }
            else
            {
                alarmToolStripMenuItem.Text = "Alarm";
                pictureBox4.Visible = false;
                timer1.Enabled = false;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (alarmtime.ToString("dd.MM.yyyy hh:mm") == DateTime.Now.ToString("dd.MM.yyyy hh:mm"))
            {
                timer1.Enabled = false; 
                System.Media.SystemSounds.Beep.Play();
                TopMost = true;
                MessageBox.Show("Do you remember this note?", "StickyPad - Alarm", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

    }
}
